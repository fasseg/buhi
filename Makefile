CC=gcc
CFLAGS=-I. -lX11 -lGL -lGLU -lglut -lm

all: buhi.o
	$(CC) ${CFLAGS} -o buhi buhi.o

clean:
	rm buhi.o
	rm buhi
