#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <assert.h>
#include <unistd.h>
#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string.h>
#include <math.h>

#define NIL (0)


// data model structures
typedef struct{
    float x;
    float y;
    float z;
}Vector3f;

typedef struct{
    int type;
    Vector3f pos;
    Vector3f face;
    float width;
    float height;
    float depth;
}Box;

// function prototypes
void render_scene();
void render_box(Box b);
void render_floor();
//void rotateCamera(int x, int y);
Bool check_event(Display *, XEvent *, char* arg);
Box create_box(Vector3f pos, float width, float height, float depth);
float magnitude(Vector3f *vector);
void rotate_los(Vector3f *vector, float angle_y);
void normalize(Vector3f *vector);
Vector3f cross_product(Vector3f *a, Vector3f *b);
void print_vector(Vector3f *vector, char * title);
void set_vector_null(Vector3f *vector);

// global variables
static const int SHAPE_BOX = 1;
Vector3f velocity = {.x=0, .y=0, .z=0};
Vector3f camera = {.x=0, .y=0, .z=-5};
Vector3f los = {.x=0,.y=0,.z=1};
Vector3f world_up = {.x=0, .y=1, .z=0};
static const int WIDTH = 600;
static const int HEIGHT = 600;
float anglex;
int last_mouse_pos_x;
float smooth_rot_x;
static const int FLAG_MOVE_FORWARD = 1;
static const int FLAG_MOVE_BACKWARDS = 2;
static const int FLAG_MOVE_LEFT = 4;
static const int FLAG_MOVE_RIGHT = 8;
int movement_flags = 0;

int main(char** argv, int argc){
    Display *dsp = XOpenDisplay(NIL);
    assert(dsp);

    int colorBlack = BlackPixel(dsp,DefaultScreen(dsp));
    int colorWhite = WhitePixel(dsp,DefaultScreen(dsp));

    Window root = DefaultRootWindow(dsp);

    GLint att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
    XVisualInfo *vi = glXChooseVisual(dsp, 0, att);

    // check if visual is available
    if (vi == NULL) {
        printf("\n\tno appropriate visual found\n\n");
        exit(0);
    } else{
        printf("\n\tvisual %p selected\n", (void *)vi->visualid);
    }

    Colormap cmap = XCreateColormap(dsp, root, vi->visual, AllocNone);

    XSetWindowAttributes xsAttrs;
    xsAttrs.colormap = cmap;
    xsAttrs.event_mask = ExposureMask | KeyPressMask;

    Window wnd = XCreateWindow(dsp, root, 0, 0, WIDTH, HEIGHT, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask, &xsAttrs);
    XMapWindow(dsp, wnd);
    XStoreName(dsp, wnd, "Buhi!");

    XWindowAttributes winAttrs;
    int screenNumber = DefaultScreen(dsp);

    GLXContext glx = glXCreateContext(dsp, vi, NULL, GL_TRUE);
    glXMakeCurrent(dsp, wnd, glx);
    glEnable(GL_DEPTH_TEST);

    // disable auto repeat
    Bool detectableAutoRepeatSupported;
    XkbSetDetectableAutoRepeat(dsp, True, &detectableAutoRepeatSupported);
    printf("detectable autorepeat supported: %d\n",detectableAutoRepeatSupported); 

    // registering listener for interesting events
    XSelectInput(dsp, wnd, KeyPressMask | KeyReleaseMask | ExposureMask | PointerMotionMask);


    Bool exposed = False;
    printf("\ninitialization done\n");

    for (;;) {
        XEvent e;
        int code;
        if (XCheckIfEvent(dsp,&e,check_event,"1") == True){
            if (e.type == Expose) {
                exposed = True;
                XGetWindowAttributes(dsp, wnd, &winAttrs);
                glViewport(0, 0, winAttrs.width, winAttrs.height);
                // and query the mouse cursor for the original position
                int root_x,root_y, last_mouse_pos_y;
                unsigned int ret_mask;
                Window child_ret;
                XQueryPointer(dsp, wnd, &root, &child_ret,&root_x, &root_y, &last_mouse_pos_x, &last_mouse_pos_y, &ret_mask);
                render_scene(); 
                glXSwapBuffers(dsp,wnd);
            }else if (e.type == KeyPress || e.type == KeyRelease){
                code = XLookupKeysym(&e.xkey, 0);
                switch (code){
                    case XK_Escape:
                        glXMakeCurrent(dsp, None, NULL);
                        glXDestroyContext(dsp, glx);
                        XDestroyWindow(dsp, wnd);
                        XCloseDisplay(dsp);
                        exit(0);
                        break;
                    case XK_w:
                        if (e.type == KeyPress){
                            movement_flags |= FLAG_MOVE_FORWARD;
                        }else{
                            movement_flags &= ~FLAG_MOVE_FORWARD;
                        }
                        break;
                    case XK_s:
                        if (e.type == KeyPress){
                            movement_flags |= FLAG_MOVE_BACKWARDS;
                        } else {
                            movement_flags &= ~FLAG_MOVE_BACKWARDS;
                        }
                        break;
                    case XK_d:
                        if (e.type == KeyPress) {
                            movement_flags |= FLAG_MOVE_RIGHT;
                        }else{
                            movement_flags &= ~FLAG_MOVE_RIGHT;
                        }
                        break;
                    case XK_a:
                        if (e.type == KeyPress) {
                            movement_flags |= FLAG_MOVE_LEFT;
                        }else{
                            movement_flags &= ~FLAG_MOVE_LEFT;
                        }
                        break;
                    default:
                        break;
                }
            }else if (e.type == MotionNotify){
                int px = e.xmotion.x - last_mouse_pos_x;
                last_mouse_pos_x = e.xmotion.x;
                smooth_rot_x += px;
            }
        }else if (exposed == True){
            // calculate the angle between los and frame
            float angle = 
                //usleep(10000);
                camera.x = camera.x + velocity.x;
            render_scene();
            glXSwapBuffers(dsp,wnd);
            char fps[20];
            sprintf(fps,"V = %.2f",velocity.z); 
            char direction[20];
            sprintf(direction, "Angle: %f",anglex);
            XColor fg;
            char colorString[] = "#00ff00";
            GC textGC = XCreateGC(dsp, wnd, 0, 0);
            XParseColor(dsp, cmap, colorString, &fg);
            XAllocColor(dsp, cmap, &fg);    
            XSetForeground(dsp, textGC, fg.pixel);
            XDrawString(dsp, wnd, textGC, 5, 15, fps, strlen(fps));
            XDrawString(dsp, wnd, textGC, 5, 30, direction, strlen(direction));
        }

    }
    return 0;
}

void set_vector_null(Vector3f *vector){
    vector->x=0.0;
    vector->y=0.0;
    vector->z=0.0;
}

void scale(Vector3f *vector, float factor){
        vector->x = factor * vector->x;
        vector->y = factor * vector->y;
        vector->z = factor * vector->z;
}

Vector3f cross_product(Vector3f *a, Vector3f *b){
    Vector3f cross;
    cross.x = a->z * b->y - a->y * b->z;
    cross.y = a->z * b->x - a->x * b->z;
    cross.z = a->x * b->y - a->y * b->y;
    return cross;
}

void rotate_los(Vector3f *vector, float angle_y){
    angle_y = angle_y * M_PI/180;
    printf("rotating by %f degrees\n", angle_y * 180/M_PI);
    float x = 1 * sin(angle_y);
    float y = 0;
    float z = 1 * cos(angle_y);
    vector->x=x;
    vector->y=y;
    vector->z=z;
}

void normalize(Vector3f *vector){
    float mag = magnitude(vector); 
    vector->x = vector->x/mag;
    vector->y = vector->y/mag;
    vector->z = vector->z/mag;
}

void print_vector(Vector3f *vector, char *title){
    printf("%s: [%.2f %.2f %.2f] Mag: %.2f\n",title,vector->x,vector->y,vector->z, magnitude(vector));
}

float magnitude(Vector3f *vector){
    return sqrt((vector->x * vector->x) + (vector->y * vector->y) + (vector->z * vector->z));
}
void render_scene(){
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.0, 1.0, 1.0, 10.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // smooth look via mouse
    if (smooth_rot_x > 1) {
        float step = 1.0 + smooth_rot_x/2.0;
        smooth_rot_x = smooth_rot_x - step;
        anglex = anglex + step;
        rotate_los(&los, anglex);
        normalize(&los);
        print_vector(&los, "LOS");
    }else if (smooth_rot_x < -1){
        float step = -1.0 + smooth_rot_x/2.0;
        smooth_rot_x = smooth_rot_x - step;
        anglex = anglex + step;
        rotate_los(&los,-anglex);
        normalize(&los);
        print_vector(&los, "LOS");
    }

    glRotatef(anglex, 0, 1, 0);

    // calulate movement
    normalize(&los);
    set_vector_null(&velocity);
    if ((movement_flags & FLAG_MOVE_FORWARD) == FLAG_MOVE_FORWARD){
        velocity.x = los.x * 0.1;
        velocity.z = los.z * 0.1;
    }
    if ((movement_flags & FLAG_MOVE_BACKWARDS) == FLAG_MOVE_BACKWARDS){
        velocity.x = velocity.x - los.x * 0.1;
        velocity.z = velocity.z - los.z * 0.1;
    }
    if ((movement_flags & FLAG_MOVE_RIGHT) == FLAG_MOVE_RIGHT){
        Vector3f cross = cross_product(&los,&world_up);
        normalize(&cross);
        velocity.x = velocity.x - cross.x * 0.1;
        velocity.z = velocity.z + cross.z * 0.1;
    }
    if (( movement_flags & FLAG_MOVE_LEFT) == FLAG_MOVE_LEFT){
        Vector3f cross = cross_product(&los,&world_up);
        normalize(&cross);
        velocity.x = velocity.x + cross.x * 0.1;
        velocity.z = velocity.z + cross.z * 0.1;
    }
    if (magnitude(&velocity) > 0.0){
        normalize(&velocity);
        scale(&velocity,0.1);
    }
    camera.x = camera.x + velocity.x;
    camera.z = camera.z + velocity.z;

    glTranslatef(camera.x, camera.y, camera.z);

    Vector3f pos = (Vector3f) {.x=0.0, .y=0.0, .z=0.0};
    Box b = create_box(pos,3.0, 3.0, 3.0);

    render_box(b);
    render_floor();
}

void render_box(Box b){
    glBegin(GL_QUADS);
    // back face
    glColor3f(0.5, 1.0, 1.0);
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z-0.5*(b.depth));
    // right face
    glColor3f(1.0, 0.5, 1.0);
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z+0.5*(b.depth));
    // left face
    glColor3f(1.0, 1.0, 0.5);
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z+0.5*(b.depth));
    // top face
    glColor3f(1.0, 0.5, 0.5);
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z+0.5*(b.depth));
    // bottom face
    glColor3f(0.5, 0.5, 1.0);
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z-0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z+0.5*(b.depth));
    // front face
    glColor3f(0.5, 1.0, 0.5);
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y+(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x+(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z+0.5*(b.depth));
    glVertex3f(b.pos.x-(0.5*b.width), b.pos.y-(0.5*b.height), b.pos.z+0.5*(b.depth));
    glEnd();
}

void render_floor(){
    glColor3f(0.7,0.7,0.7);
    glBegin(GL_QUADS);
    glVertex3f(-10.0, -1.0, -10.0);
    glVertex3f(10.0, -1.0, -10.0);
    glVertex3f(10.0,-1.0, 10.0);
    glVertex3f(-10.0, -1.0, 10.0);
    glEnd();
}

// a procedure to catch events without blocking
Bool check_event(Display *dsp, XEvent *e, char *arg){
    int code;
    switch (e->type){
        case Expose:
            return True;
        case KeyPress:
            return True;
        case KeyRelease:
            return True;
        case MotionNotify:
            return True;
        default:
            return False;
    }
}

// create and return an empty box shape
Box create_box(Vector3f pos, float width, float height, float depth){
    Box b = {.type=SHAPE_BOX, .pos=pos, .width=width, .height=height, .depth=depth};
    return b;
}
